"""
Напишите программу-калькулятор, которая поддерживает следующие операции: сложение, вычитание, умножение, деление
и возведение в степень. Программа должна выдавать сообщения об ошибке и продолжать работу при вводе некорректных
данных, делении на ноль и возведении нуля в отрицательную степень.
"""


class WrongIndentation(ValueError):
    def __str__(self):
        return "wrong indentation"


class Parser:
    def __init__(self):
        pass

    @staticmethod
    def __convert_type(value_str):
        result = 0
        if '.' in value_str:
            try:
                result = float(value_str)
            except ValueError:
                print("invalid literal for float()")
                raise
        else:
            try:
                result = int(value_str)
            except ValueError:
                print("invalid literal for int()")
                raise
        return result

    def parse(self, expression):
        packed_values = tuple(expression.split(' '))

        if len(packed_values) != 3:
            raise WrongIndentation

        a, op, b = packed_values

        try:
            a_num = self.__convert_type(a)
        except ValueError:
            raise
        try:
            b_num = self.__convert_type(b)
        except ValueError:
            raise

        return a_num, op, b_num


class Core:
    def __init__(self):
        self._parser = Parser()
        self._functions = {
            "+": lambda a, b: a + b,
            "-": lambda a, b: a - b,
            "*": lambda a, b: a * b,
            "/": lambda a, b: a / b,
            "**": lambda a, b: a ** b

        }

    def calculate(self, expression):
        result = None
        try:
            a, op, b = self._parser.parse(expression)
        except WrongIndentation as exception:
            print(exception)
            print("Check your expression!")
        except ValueError:
            print("Check your expression!")
        else:
            func = self._functions.get(op, None)
            if not func:
                print("this operation is not determinate!")
            else:
                try:
                    result = func(a, b)
                except ZeroDivisionError as exception:
                    print(exception)
        return result


class Interface:
    def __init__(self):
        self._core = Core()

    def run_calculator(self):
        while True:
            print("Enter expression, for ex. '2 + 2' or 'q' for exit: ")
            expression = input()
            if expression == 'q':
                break

            result = self._core.calculate(expression)
            print(f"Result: {result}")
            print('='*10)


if __name__ == "__main__":
    calculator = Interface()
    calculator.run_calculator()
