"""
Опишите свой класс исключения. Напишите функцию, которая будет выбрасывать данное исключение, если пользователь
введёт определённое значение, и перехватите это исключение при вызове функции.
"""


class MyException(Exception):
    def __str__(self):
        return "invalid value"


def get_my_exception():
    value = input("Input value: ")
    if value == 'err':
        raise MyException


if __name__ == "__main__":
    try:
        get_my_exception()
    except MyException as exception:
        print(exception)
