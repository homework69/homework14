"""
Опишите класс сотрудника, который включает в себя такие поля, как имя, фамилия, отдел и год поступления на работу.
Конструктор должен генерировать исключение, если заданы неправильные данные. Введите список работников с клавиатуры.
Выведите всех сотрудников, которые были приняты после заданного года.
"""

from datetime import date
# import datetime


class Employee:
    DEPARTMENTS = {
        '1': 'Адміністрація',
        '2': 'Госп. відділ',
        '3': 'ІТ',
        '4': 'Відділ продажів'
    }

    def __init__(
            self,
            firstname: str,
            lastname: str,
            department_id: str,
            year_str: str
    ):
        is_error = False
        str_error = ""
        if not firstname:
            is_error = True
            str_error += ("" if str_error == "" else "\n") + "value firstname cannot be empty"
        if not lastname:
            is_error = True
            str_error += ("" if str_error == "" else "\n") + "value lastname cannot be empty"
        if not year_str.isdigit():
            is_error = True
            str_error += ("" if str_error == "" else "\n") + "value year_str must be digital and not empty"

#        if not department:
#            is_error = True
#            str_error += ("" if str_error == "" else "\n") + "value department cannot be empty"

        if is_error:
            raise ValueError(str_error)

        self.firstname = firstname.capitalize()
        self.lastname = lastname.capitalize()
        self.department = Employee.DEPARTMENTS.get(department_id, 'Стажування')
        self.year_start = date(int(year_str), 1, 1)

    def __str__(self):
        return f"{self.firstname} {self.lastname} work in department {self.department} from {self.year_start}"

    def output_employees_from(self, year_from):
        if not year_from.isdigit():
            str_error = "value year_from must be digital and not empty"
            raise ValueError(str_error)
        if self.year_start >= date(int(year_from), 1, 1):
            print(self)


class Interface:
    def __init__(self):
        self.data_employee = []

    def run_input(self):
        while True:

            print("Enter Firstname of employee or 'q' for exit: ")
            firstname = input()

            if firstname == 'q':
                command = input("Enter 'u' for output / 'q' for exit / 'c' for continue: ")
                if command == 'q':
                    break
                elif command == 'u':
                    self.run_output()
                    continue
                else:
                    continue

            print("Enter Lastname: ")
            lastname = input()
            print("Change department: \n"
                  "'1': 'Адміністрація'\n"
                  "'2': 'Госп. відділ'\n"
                  "'3': 'ІТ'\n"
                  "'4': 'Відділ продажів'\n"
                  "'<other>': 'Стажування'")
            department_id = input()
            print("Enter year of employment: ")
            year_str = input()

            try:
                self.data_employee.append(
                    Employee(
                        firstname=firstname,
                        lastname=lastname,
                        department_id=department_id,
                        year_str=year_str
                    )
                )
            except ValueError as exception:
                print(exception)
                print("employee has not been added!")
                continue
            finally:
                print("="*20)

    def run_output(self):
        year_from = input("Input year from: ")
        for employee in self.data_employee:
            employee.output_employees_from(year_from)
        print("=" * 20)


if __name__ == "__main__":
    data_interface = Interface()
    data_interface.run_input()
